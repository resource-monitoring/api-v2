import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Injectable } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';

import { PageViewDto } from './page-view.dto';

import { PageView, PageViewDocument } from './page-view.schema';

@Injectable()
export class PageViewService {
    private ES_INDEX: string = 'page-view';

    constructor(
        @InjectModel(PageView.name) private pageViewModel: Model<PageViewDocument>,
        private readonly elasticsearchService: ElasticsearchService
    ) {}

    private index(pageViewDto: PageViewDto) {
        return this.elasticsearchService.index<PageViewDto>({
            index: this.ES_INDEX,
            body: pageViewDto,
        })
    }

    public async save(pageViewDto: PageViewDto): Promise<PageView> {
        const pageView = new this.pageViewModel(pageViewDto);
        await this.index(pageViewDto);
        return pageView.save();
    }
}
