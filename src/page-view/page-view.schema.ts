import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

import { Browser, Cpu, Device, Engine, Os } from './page-view.dto';

export type PageViewDocument = PageView & Document;

@Schema()
export class PageView {
    @Prop()
    ua: string;

    @Prop({ type: Map })
    browser: Browser;

    @Prop({ type: Map })
    engine: Engine;

    @Prop({ type: Map })
    os: Os;

    @Prop({ type: Map })
    device: Device;

    @Prop({ type: Map })
    cpu: Cpu;
}


export const PageViewSchema = SchemaFactory.createForClass(PageView);