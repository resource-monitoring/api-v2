export type Browser = {
    name: string;
    version: string;
    major: string;
}

export type Engine = {
    name: string;
    version: string;
}
export type Os = {
    name: string;
    version: string;
}

export type Device = {
    model: string;
    type: string;
    vemdor: string;
}

export type Cpu = {
    architecture: string;
}

export class PageViewDto {
    ua: string;
    browser: Browser;
    engine: Engine;
    os: Os;
    device: Device;
    cpu: Cpu;
}