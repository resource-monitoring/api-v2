import { Body, Controller, Post } from '@nestjs/common';

import { PageViewDto } from './page-view.dto';
import { PageViewService } from './page-view.service';
import { PageView } from './page-view.schema';

@Controller('page-view')
export class PageViewController {
    constructor(private pageViewService: PageViewService) {}

    @Post()
    save(@Body() pageView: PageViewDto): Promise<PageView> {
        return this.pageViewService.save(pageView);
    }
}
