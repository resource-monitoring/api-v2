import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ElasticsearchModule } from '@nestjs/elasticsearch';

import { PageViewController } from './page-view/page-view.controller';
import { PageViewService } from './page-view/page-view.service';

import { PageView, PageViewSchema } from './page-view/page-view.schema';

@Module({
  imports: [
    ElasticsearchModule.register({
      node: process.env.ELASTICSEARCH_URL,
    }),
    MongooseModule.forRoot(process.env.MONGO_URL),
    MongooseModule.forFeature([
      { name: PageView.name, schema: PageViewSchema }
    ]),
  ],
  controllers: [PageViewController],
  providers: [PageViewService],
})
export class AppModule {}
